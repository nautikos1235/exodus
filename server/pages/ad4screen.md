# Ad4Screen
## La société
Ce tracker est édité par **Ad4Screen** - **Mobile First Alliance** présidée par [*Jérôme Stioui*](https://frama.link/yQ8zsQpT)

[Réseau de Ad4Screen](http://french-leader.com/report.php?siret=53851303700019)

### Notes éthiques
  * Retargeting
  * Notificatiosn Push
  * Programmes Relationnels
  * Email Responsive
  * Passbook
  * RTB
  * Tracking
  * App Store SEO
  * Opérations Evenementielles
  * Campagne de Branding
  * M Commerce
  * Chatbots
  * Classified
  * BackInApp

## Données potentiellement collectées
Pour obtenir cette liste, se rendre sur [`https://api.accengage.com/routes`](https://api.accengage.com/routes).
```
BMA4SAuthentication : https://api.accengage.com/v1/sdk_access_token"
BMA4SBulk : https://apiproxy.accengage.com/v1/_bulk"
BMA4SVersion : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/version"
BMA4SNotificationToken : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/token"
BMA4SPrivateTracker : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/open"
BMA4SConfig : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/config"
BMA4SGeoLoc : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/geolocs"
BMA4SGeoLocs : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/geolocs"
BMA4SGeofencing : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/geofences"
BMA4SGeofencingUpdate : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/geofences"
BMA4SEvent : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/events"
BMA4SLead : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/lead"
BMA4SCart : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/cart"
BMA4SPurchase : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/purchase"
BMA4SUpdateDeviceFields : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/fields"
BMA4SUpdateMemberFields : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/members/|memberId|/fields"
BMA4SNotificationTracking : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/track/push"
BMA4SInAppTracking : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/track/inapp"
BMA4SInboxTracking : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/track/inbox"
BMA4SBeacons : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/beacons"
BMA4SBeaconsUpdate : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/beacons"
BMA4SReferrer : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/referrer"
BMA4SOpenPushWeb : https://api.accengage.com/v1/apps/|partnerId|/browser/|sharedId|/open"
BMA4SFetchPushWeb : https://api.accengage.com/v1/apps/|partnerId|/browser/|sharedId|/fetch"
BMA4SList : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/lists"
BMA4SListListing : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/lists/listing"
BMA4SPermissions : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/permissions"
BMA4SPermissionsUpdate : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/permissions"
BMA4SNotificationTokenWeb : https://api.accengage.com/v1/apps/|partnerId|/browser/|sharedId|/token"
BMA4SMessageTags : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/tags"
BMA4SInAppNotificationV2 : https://api.accengage.com/v1/apps/|partnerId|/devices/|sharedId|/inapp"
BMA4SNextGen : http://trk.a4.tl"
BMA4SEventGoogleAnalytics : https://www.google-analytics.com/collect"
BMA4SUploadFacebookProfile : http://trk.a4.tl/api/facebook/"
BMA4SCrowd : http://applist.a4.tl"
BMA4SPrivateWebViewTracker : http://apptrk.a4.tl/wtrk.php?"
BMA4SProcessReport : http://applist.a4.tl/"
BMA4SURLSchemeExplorer : http://applist.a4.tl/"
BMA4SInAppNotification : https://api.ad4push.com/inAppConfigJson.php"
BMA4SInboxList : https://api.ad4push.com/inboxMessageList.php"
BMA4SInboxDetails : https://api.ad4push.com/inboxMessageDetail.php"
BMA4SInboxUpdate : https://api.ad4push.com/inboxMessageStatus.php"
BMA4SMemberLink : https://api.ad4push.com/linkMember.php"
BMA4SMemberUnlink : https://api.ad4push.com/unlinkMembers.php"
BMA4SMemberList : https://api.ad4push.com/listMembers.php"
BMA4SWebviewScriptUrl : https://apptrk.a4.tl/api/event/?partnerId=|partnerId|"}]}
```

### Preuve
![1](/static/content/img/oGBKDSPAJRyX.png)

## Destination des données
`89.185.56.27` IP localisée à Nateuil-les-Meaux (77))
Les données de géolocalisation passent par la filliale Accengage pour arriver dans un serveur en [Californie appartenant à Google](http://whois.domaintools.com/accengage.com). 

### Preuve
![1](/static/content/img/mbOnQ0jf58gk.png)


## Règle de détection
  * `ad4screen`
  * `apptrk\\.a4\\.tl`
  * `app4free\\.com`

# Filiales et clients
## Ad4Screen
  * PriceMinister (Rakuten Group)
  * OTC Agregator
  * Nestlé Bébé
  * National Citer
  * LILIGO
  * La Redoute
  * Buffalo Grill
  * Europcar <-- CONFIRMÉ
  * AirFrance
  * Vente-Privée
  * Pages Jaunes
  * Casino
  * Voyages-SNCF.com
  * The Walt Disney Company
  * Club Med
  * Century 21
  * Betclic
  * Bouygues Telecom
  * SFR
  * Numericable
  * ebooekers
  * La Tribune
  * Le Figaro
  * Universal Music
  * AXA
  * Danone
  * Saxo Banque
  * Capcom
  * Cadremploi.fr
  * Nexity
  * Pôle EmploiL
  * RocketInternet
  * Condé Nast
  * TF1
## Relatia (filiale de Ad4Screen)
  * 3 Suisses
  * Peugeot
  * Transilien
  * Celio
  * Carrefour Drive
  * Sephora
  * Toyota
  * AFMThéléthon
  * Orange
  * Oney
  * Nina Ricci
  * Malakoff Médéric
  * Les Petits Bilingues
  * Groupama
  * FranceTélévisions
  * EDF
  * Engie
  * Compagnie de Phalsbourg
  * Clarins
  * Camaieu
  * BNP PAribas
  * Auchan Drive
  * American Express
## LinkDispatcher (service de Relatia)
  * Pages Jaunes
  * FDJ
  * 3 Suisses
  * La Redoute
  * TrendCorner
  * Celio
  * PagesJaunes
  * FFF
## Accengage (filiale de Ad4Screen)
  * Backelite
  * Niji
  * Buisness lab
  * itelios
  * accenture
  * Buisness And Descision
  * Keyrus
  * GenerationUP
  * InBox
  * Groupe Estia
  * Tealium
  * Insign
  * Capgemini
  * pictime
  * Wazapp
  * WEmanity
  * Adneom
  * airweb
  * Bolero
  * soft.computing
  * aigily
  * DigitasLBi
  * PhoneValley
  * Snapp'
  * Aid
  * Velvet
  * Flixbus
  * otto group
  * Alibaba Group
  * Orange