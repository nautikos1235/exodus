# Teemo
## La société
Ce tracker est édité par la société française Teemo.

Autres infos sur Teemo :

  * [http://www.numerama.com/politique/282934-enquete-comment-les-apps-figaro-lequipe-ou-closer-participent-au-pistage-de-10-millions-de-francais.html](http://www.numerama.com/politique/282934-enquete-comment-les-apps-figaro-lequipe-ou-closer-participent-au-pistage-de-10-millions-de-francais.html)
  * [http://www.numerama.com/politique/283693-les-inconnues-du-dossier-teemo-quelques-pistes-pour-poursuivre-la-reflexion.html](http://www.numerama.com/politique/283693-les-inconnues-du-dossier-teemo-quelques-pistes-pour-poursuivre-la-reflexion.html)
  * autres infos sur teemo : [http://www.digitalcmo.fr/databerries-revolutione-le-drive-to-store-avec-son-real-life-targeting/](http://www.digitalcmo.fr/databerries-revolutione-le-drive-to-store-avec-son-real-life-targeting/)

### Notes éthiques
  * Drive-to-store
  * Données envoyées aux USA

## Données potentiellement collectées
Les datas collectées partent sur `https://v1.blueberry.cloud.databerries.com/mobile_backend` :

  * latitude
  * longitude
  * radius
  * timestamp
  * uuid

### Preuves
![1](/static/content/img/1wSeqlBCOpuM.png)
![1](/static/content/img/ThfrVKssXv7b.png)

## Destination des données
Le SDK de Teemo utilise l'URL `v1.blueberry.cloud.databerries.com` localisée en Californie.

  * Google Cloud Platform
  * Serveurs en Californie

### Preuve 
![1](/static/content/img/dPl6FcB3ELgp.png)

## Règle de détection
  * `databerries`

## Applications 
  * `fr.meteo` au moins 5000000 téléchargements
  * `lequipe.fr` au moins 5000000 téléchargements
  * `redshift.closer` au moins 1000000 téléchargements
  * `fr.playsoft.teleloisirs` au moins 5000000 téléchargements
  * `com.allocine.androidapp` au moins 5000000 téléchargements
  * `com.weather.Weather` au moins 50000000 téléchargements
  * `com.mondadori.telestar` au moins 100000 téléchargements
  * `com.webwag.topsante` au moins 100000 téléchargements
  * `fmlife.activities` au moins 1000000 téléchargements
  * `fr.snapp.fidme` au moins 1000000 téléchargements
  * `smartmobilesoftware.flightstatus2trial` au moins 1000000 téléchargements
  * `com.ismaker.android.simsimi` au moins 50000000 téléchargements
  * `redshift.grazia` au moins 100000 téléchargements
  * `com.aufeminin.marmiton.activities` au moins 5000000 téléchargements
  * `com.toj.gasnow` au moins 100000 téléchargements
  * `com.digidust.elokence.akinator.freemium` au moins 50000000 téléchargements
  * `com.emas.autojournal` au moins 100000 téléchargements
  * `com.emas.autoplus` au moins 500000 téléchargements
  * `vdm.activities` au moins 1000000 téléchargements
  * `com.arcsoft.perfect365` au moins 10000000 téléchargements

Soit 191 000 000 de téléchargements au total.

**NB** le nombre de téléchargements est comptabilisé par Google Play à partir de la première publication de l'appli.